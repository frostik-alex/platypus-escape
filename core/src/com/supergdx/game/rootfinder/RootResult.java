package com.supergdx.game.rootfinder;

/**
 * Created by alexey.moroz on 3/19/18.
 */


public class RootResult {
    private static final float DEFAULT_ROOT = 0.0f;

    private RootStatus status;
    private float root;


    public static RootResult FAIL = new RootResult(RootStatus.FAIL, DEFAULT_ROOT);

    public static RootResult SUCCESS(float rootValue){
        return new RootResult(RootStatus.OK, rootValue);
    }

    private RootResult(RootStatus status, float root) {
        this.status = status;
        this.root = root;
    }

    public RootStatus getStatus(){
        return status;
    }

    public float getRootValue(){
        return this.root;
    }
}
