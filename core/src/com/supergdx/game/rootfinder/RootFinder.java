package com.supergdx.game.rootfinder;


import com.supergdx.game.math.MFunction;

/**
 * Created by alexey.moroz on 3/19/18.
 */


public class RootFinder {

    private static final int MAX_ITERATION_COUNT = 400;

    public static RootResult newtonsApproxRoot(MFunction mFunc, float initGuess, float accuracy) {

        int counter = 0;

        //previous guess
        float prevGuess = initGuess;

        //current guess
        float currGuess;

        while (counter < MAX_ITERATION_COUNT) {

            //calculate next iteration guess.
            currGuess = prevGuess - mFunc.val(prevGuess)/mFunc.der(prevGuess);


            //check if we've reached accuracy level and if so, check root for validity
            if( isAccuracyReached(currGuess, prevGuess, accuracy) && isValidRoot(mFunc, currGuess, accuracy)){
                return RootResult.SUCCESS(currGuess);
            }

            //in case we've not succeed yet, remember result for future iteration
            prevGuess = currGuess;

            counter++;
        }


        //return FAIL result in case when we've run out of MAX ITERATION COUNT.
        return RootResult.FAIL;
    }



    private static boolean isValidRoot(MFunction mFunc, float rootValue, float accuracy) {
        /**
         * If the signs of f(xn+ϵ) and f(xn−ϵ) are opposites of each other, then the accuracy of xn is verified and stop the algorithm.
         * xn is a good approximation to α.
         * If the signs of f(xn+ϵ) and f(xn−ϵ) are the same, then α is not contained in the small interval [xn−ϵ,xn+ϵ].
         */
        return mFunc.val(rootValue + accuracy) * mFunc.val(rootValue - accuracy) < 0.0f;
    }


    private static boolean isAccuracyReached(float currGuess, float prevGuess, float accuracy){
        return Math.abs(currGuess - prevGuess) < accuracy;
    }

}
