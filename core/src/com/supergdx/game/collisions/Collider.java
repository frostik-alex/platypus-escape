package com.supergdx.game.collisions;


import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexey.moroz on 3/21/18.
 */

public class Collider {


    private List<Collidable> collidablesList;

    public Collider(){
        collidablesList = new LinkedList<Collidable>();
    }

    public void addCollidable(Collidable collidable){
        collidablesList.add(collidable);
    }


    public void checkAndPropagateCollisions(){

        for(Collidable collidable: collidablesList){

            for(Collidable otherCollidable: collidablesList){
                //TODO: Check if needed.
            }
        }
    }



}
