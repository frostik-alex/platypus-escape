package com.supergdx.game.collisions;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by alexey.moroz on 4/7/18.
 */

public class CollisionResult {

    public boolean colliding;
    public Vector2 charPos;
    public Vector2 closestPoint;

    public static final CollisionResult NO_COLLISION = new CollisionResult(false,  null,null, null);

    public Vector2 collisionVec;

    private CollisionResult(boolean colliding, Vector2 charPos, Vector2 collisionVec, Vector2 closestPoint) {
        this.colliding = colliding;
        this.collisionVec = collisionVec;
        this.closestPoint = closestPoint;
        this.charPos = charPos;
    }

    public static CollisionResult collisionDetected(Vector2 charPos, Vector2 collisionVec, Vector2 closestPoint){
        return new CollisionResult(true, charPos, collisionVec, closestPoint);
    }

}
