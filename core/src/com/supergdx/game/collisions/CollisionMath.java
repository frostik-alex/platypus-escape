package com.supergdx.game.collisions;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.supergdx.game.math.MFunction;
import com.supergdx.game.math.functions.ShortestDistanceFunction;
import com.supergdx.game.rootfinder.RootFinder;
import com.supergdx.game.rootfinder.RootResult;
import com.supergdx.game.rootfinder.RootStatus;
import com.supergdx.game.terrain.Terrain;

/**
 * Created by alexey.moroz on 4/7/18.
 */

public class CollisionMath {

    private static final float ROOT_SOLUTION_ACC = 0.0001f;


    public static CollisionResult detectMovingObjCollisionWithTerrain(Vector2 prevPos, Vector2 currPos, float radius, Terrain terrain){

        float startX = prevPos.x;
        float startY = prevPos.y;

        float checkStep = radius/2.0f;
        Vector2 dirVec = currPos.cpy().sub(prevPos).nor();
        float distanceTraveled = currPos.dst(prevPos);
        int stepsCount = (int) Math.floor(distanceTraveled / checkStep);

        for(int i=0; i<=stepsCount; i++){
            float checkX = startX + dirVec.x * i * checkStep;
            float checkY = startY + dirVec.y * i * checkStep;

            CollisionResult collisionResult = checkCircleCollisionWithTerrain(checkX, checkY, radius, terrain);

            if(collisionResult.colliding){
                return collisionResult;
            }
        }


        return detectCollisionWithTerrain(new Circle(currPos, radius), terrain);
    }

    private static boolean isPointInCircle(float px, float py, float cx, float cy, float cr){
        float dx = px - cx;
        float dy = py - cy;

        return dx * dx + dy * dy <= cr * cr;
    }

    private static float distBetweenPoints(float p1x, float p1y, float p2x, float p2y){
        float dx = p1x - p2x;
        float dy = p1y - p2y;

        return (float) Math.sqrt(dx*dx + dy*dy);
    }

    public static CollisionResult checkCircleCollisionWithTerrain(float cx, float cy, float cr, Terrain terrain){
        MFunction terrainFunc = terrain.getFunction();
        ShortestDistanceFunction shortDistFunc = terrain.getShortestDistanceFunction();

        //update a point, from which we search for a shortest distance
        shortDistFunc.setPoint(cx, cy);

        //Solve an equation of "shortDistFunc(x) = 0". The root will be X value of the terrain function.
        //This X value will also be the X value of point of intersection of a shortest perpendicular
        //From a given point (circle centre) to the terrain function.
        RootResult result = RootFinder.newtonsApproxRoot(shortDistFunc, cx, ROOT_SOLUTION_ACC);

        // in case we've failed to find roots or didn't reach approximation accuracy - assume we didn't collide.
        if (result.getStatus() == RootStatus.FAIL) {
            return CollisionResult.NO_COLLISION;
        }


        // point of projection of Circle center onto a given function. (shortest perpendicular from a point to a func).
        float cpx = result.getRootValue();
        float cpy = terrainFunc.val(result.getRootValue());


        // if closest point is out of Circle - there is no collision
        if (!isPointInCircle(cpx, cpy, cx, cy, cr)) {
            return CollisionResult.NO_COLLISION;
        }


        // ====================================================================
        // Calculate minimum shift vector (collision vector) - a minimum vector
        // to apply to a circle to avoid collision.
        // ====================================================================


        //vector from a center of a circle to a closest point on a function
        float distToFuncLen = distBetweenPoints(cpx, cpy, cx, cy);


        float collisionVecLen = cr - distToFuncLen;

        //if circle radius is less then distance - then there is no collision.
        if(collisionVecLen < 0.0f){
            return CollisionResult.NO_COLLISION;
        }

        //normalize distToFunc vector and multiply by -collisionVecLen (minus needed to revert to opposite direction).
        return CollisionResult.collisionDetected(
                new Vector2(cx, cy),
                new Vector2(
                        collisionVecLen * (cx - cpx) / distToFuncLen,
                        collisionVecLen * (cy - cpy) / distToFuncLen
                ),
                new Vector2(cpx, cpy)
        );
    }



    public static CollisionResult detectCollisionWithTerrain(Circle circleShape, Terrain terrain) {
        return checkCircleCollisionWithTerrain(circleShape.x, circleShape.y, circleShape.radius, terrain);
    }

}
