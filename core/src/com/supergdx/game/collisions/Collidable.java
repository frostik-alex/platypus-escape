package com.supergdx.game.collisions;

import com.badlogic.gdx.math.Vector3;

/**
 * Created by alexey.moroz on 4/7/18.
 */

public interface Collidable {

    int handleCollision(Vector3 collisionVec, Vector3 reactionForceVec);

    void handleCollisionEnd(int forceUid);
}
