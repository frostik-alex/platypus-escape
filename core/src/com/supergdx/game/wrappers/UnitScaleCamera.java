package com.supergdx.game.wrappers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;

/**
 * Camera that zooms to a point when the lower
 *
 * Created by alexey.moroz on 3/20/18.
 */



public class UnitScaleCamera extends OrthographicCamera{

    public float scaleZoom;

    public UnitScaleCamera(int viewportWidth, int viewportHeight){
        super(viewportWidth, viewportHeight);

        this.scaleZoom = 1.0f;
        this.position.z = 2.0f;
    }



    private void updateViewport(int width, int height){
        this.viewportWidth = width;
        this.viewportHeight = height;

        float viewportZoom = 2.0f/Math.min(this.viewportWidth, this.viewportHeight);

        this.zoom = viewportZoom * (scaleZoom > 1.0f ? (1.0f - 1.0f / scaleZoom) : (1.0f/scaleZoom));
    }

    @Override
    public void update(){
        super.update();

        updateViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }
}
