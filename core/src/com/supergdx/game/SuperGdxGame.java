package com.supergdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.UBJsonReader;
import com.supergdx.game.collisions.CollisionMath;
import com.supergdx.game.collisions.CollisionResult;
import com.supergdx.game.math.FMath;
import com.supergdx.game.math.functions.ComplexSineFunction;
import com.supergdx.game.objects.PlayerCharacter;
import com.supergdx.game.terrain.Terrain;
import com.supergdx.game.wrappers.UnitScaleCamera;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.badlogic.gdx.graphics.GL20.GL_BLEND;
import static com.supergdx.game.objects.PlayerCharacter.CHAR_RADIUS;


public class SuperGdxGame extends ApplicationAdapter implements InputProcessor {


    public static final float CHAR_MOVE_INC = 0.05f;
    public static final float DEFAULT_ZOOM = 0.5f;


    public static final float PI_DIV_2 = (float) (Math.PI / 2.0f);

    class TouchInfo {
        public float touchX = 0;
        public float touchY = 0;
        public boolean touched = false;
    }


    private ShapeRenderer shapeRenderer;
    private UnitScaleCamera camera;
    private Environment environment;

    private PlayerCharacter character;

    private int gravityFUid = -1;
    private int terrainFUid = -1;
    private int boosterFUid = -1;

    private final static long START_TIME = System.currentTimeMillis();

    private boolean ACCELERATING = false;


    private Terrain terrain;


    private SpriteBatch batch, textBatch;


    private BitmapFont font;


    private Texture bgTexture;
    private Texture terrainTexture;
    private Texture terrainCoverTexture;


    private Sprite sprite;
    private String vertexShader;
    private String fragmentShader;
    private ShaderProgram shaderProgram;


    private Map<Integer, TouchInfo> touches = new HashMap<Integer, TouchInfo>();


    @Override
    public void create() {
        initBg();
        initTerrain();


        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();


        //create camera instance
        camera = new UnitScaleCamera(width, height);
        camera.scaleZoom = 0.5f;


        shapeRenderer = new ShapeRenderer();
        font = new BitmapFont();

        // Model loader needs a binary json reader to decode
        UBJsonReader jsonReader = new UBJsonReader();
        // Create a model loader passing in our json reader
        G3dModelLoader modelLoader = new G3dModelLoader(jsonReader);
        // Now load the model by name
        // Note, the model (g3db file ) and textures need to be added to the assets folder of the Android proj
        character = new PlayerCharacter(
                modelLoader.loadModel(Gdx.files.getFileHandle("hello_blender_alpha/hello_blender_alpha.g3db", Files.FileType.Internal))
        );
        character.setScale(0.2f);

        gravityFUid = character.setForce(gravityFUid, new Vector2(0.0f, -2.0f));


        Vector2 charPos = character.getPosition();
        charPos.y = terrain.getHeightAt(charPos.x) + 0.4f;
        character.setPosition(charPos);

        // Finally we want some light, or we wont see our color.  The environment gets passed in during
        // the rendering process.  Create one, then create an Ambient ( non-positioned, non-directional ) light.
        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 1.0f, 1.0f, 1.0f, 1.0f));

        Gdx.input.setInputProcessor(this);
        for (int i = 0; i < 5; i++) {
            touches.put(i, new TouchInfo());
        }
    }


    private void initTerrain() {
        List<ComplexSineFunction> complexSineList = new LinkedList<ComplexSineFunction>();

        complexSineList.add(
                FMath.generateComplexSineFunction(
                        new float[]{
                                //a     f       p       c
                                0.0f, 0.0f, 0.0f, 0.1f, // amplitude sine function row
                                0.0f, 0.0f, 0.0f, 1.0f, // frequency sine function row
                                0.1f, 1.0f, 0.0f, 0.5f, // phase sine function row
                                2.0f, 0.1f, 3.0f, 0.5f  // shift sine function row
                        })
        );

        complexSineList.add(

                FMath.generateComplexSineFunction(
                        new float[]{
                                0.0f, 0.0f, 0.0f, 0.1f, // amplitude sine function row
                                0.0f, 0.0f, 0.0f, 3.0f, // frequency sine function row
                                0.4f, 0.2f, 0.0f, 0.0f, // phase sine function row
                                1.2f, 1.0f, 0.0f, 0.2f  // shift sine function row
                        })
        );


        if (complexSineList.size() > 16) {
            throw new IllegalStateException("Max complex sine functions count exceeded.");
        }

        terrain = new Terrain(complexSineList);
    }


    private void initBg() {
        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();


        batch = new SpriteBatch();
        textBatch = new SpriteBatch();
//        bgTexture = new Texture("images/sky.jpg");
        bgTexture = new Texture("images/yellowsky.jpg");
        terrainTexture = new Texture("images/terrain_bubbles.jpg");
        terrainTexture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);


        terrainCoverTexture = new Texture("images/terrain_cover.png");
        terrainCoverTexture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);



        sprite = new Sprite(bgTexture);
        sprite.setSize(width, height);





        Gdx.app.log("INIT", "Bg initialized with params w:" + String.valueOf(width) + " h:" + String.valueOf(height));

        vertexShader = Gdx.files.internal("shaders/bg/bg_vert.glsl").readString();
        fragmentShader = Gdx.files.internal("shaders/bg/bg_frag.glsl").readString();
        shaderProgram = new ShaderProgram(vertexShader, fragmentShader);

        if (!shaderProgram.isCompiled())
            throw new GdxRuntimeException("Couldn't compile shader: " + shaderProgram.getLog());
        shaderProgram.pedantic = false;

    }

    @Override
    public void render() {
        renderModel();
    }


    public long getTimeSinceStart() {
        return System.currentTimeMillis() - START_TIME;
    }


    private float mod(float x, float y){
        return x - y * (float) Math.floor(x / y);
    }


    private void renderModel() {
        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        //delta time in seconds
        float dt = Gdx.graphics.getDeltaTime();

        Gdx.gl.glViewport(0, 0, width, height);
        Gdx.gl.glClearColor(0.7f, 0.7f, 0.7f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        Gdx.gl.glEnable(GL_BLEND);


        float timeSinceStartSec = getTimeSinceStart() * 0.001f;

        character.update(Gdx.graphics.getDeltaTime());


        Vector2 charPrevPos = character.getPrevPosition();
        Vector2 charCurrPos = character.getPosition();


        if(charCurrPos.y > 1.0f){
            camera.scaleZoom = DEFAULT_ZOOM * 1.0f/charCurrPos.y;
            camera.position.x = charCurrPos.x;
        }else{
            camera.position.set(charCurrPos, 0f);
            camera.scaleZoom = DEFAULT_ZOOM;
        }

        camera.update();


//        float terrainAtCharPos = terrain.getHeightAt(charCurrPos.x);

        //TODO: workaround required to handle HUGE velocities.
//        if (charCurrPos.y < terrainAtCharPos) {
//            character.setPosition(new Vector2(charCurrPos.x, terrainAtCharPos + character.boundingCircle.radius * 3f));
//            character.velocity.set(Vector2.Zero);
//            character.acceleration.set(Vector2.Zero);
//            character.boundingCircle.setPosition(charCurrPos.x,terrainAtCharPos + character.boundingCircle.radius * 3f);
//        }


        CollisionResult collResult = CollisionMath.detectMovingObjCollisionWithTerrain(charPrevPos, charCurrPos, character.boundingCircle.radius, terrain);
        Vector2 colNormVec = new Vector2();
        Vector2 terrainForceVec = new Vector2();

        if (collResult.colliding) {
            Vector2 collVec = collResult.collisionVec;

            colNormVec.set(collResult.collisionVec.cpy().nor());

            character.move(collVec);
            charCurrPos = character.getPosition();

            terrainForceVec.set(colNormVec.cpy().scl(-colNormVec.dot(character.getExclusiveResultantForce(terrainFUid))));

            character.velocity = character.velocity.sub(colNormVec.cpy().scl(colNormVec.dot(character.velocity)));
        }


        terrainFUid = this.character.setForce(terrainFUid, terrainForceVec);


        //drawing of a BG section. BG drawing is held by a shader.



        Gdx.graphics.getGL20().glActiveTexture(GL20.GL_TEXTURE2);
        terrainCoverTexture.bind();

        Gdx.graphics.getGL20().glActiveTexture(GL20.GL_TEXTURE1);
        terrainTexture.bind();

        Gdx.graphics.getGL20().glActiveTexture(GL20.GL_TEXTURE0);
        bgTexture.bind();


        batch.begin();
        batch.setShader(shaderProgram);


        shaderProgram.setUniformi("u_texture", 0);
        shaderProgram.setUniformi("u_terrain_texture", 1);
        shaderProgram.setUniformi("u_terrain_cover_texture", 2);

        shaderProgram.setUniformf("u_resolution", width, height);
        shaderProgram.setUniformf("u_aspect_ratio", ((float) Math.max(width, height) / Math.min(width, height)));
        shaderProgram.setUniformf("u_time", timeSinceStartSec);

        shaderProgram.setUniformf("u_cam_zoom", camera.scaleZoom);

        shaderProgram.setUniformf("u_cam_pos", camera.position.x, camera.position.y);

        shaderProgram.setUniformf("u_character_pos", character.getPosition().x, character.getPosition().y);
        shaderProgram.setUniformf("u_character_radius", CHAR_RADIUS);

        shaderProgram.setUniformMatrix4fv("u_terrain[0]", terrain.getParamsBuffer(), terrain.getFunctionsCount(), false);


        batch.draw(sprite, sprite.getX(), sprite.getY(), sprite.getWidth(), sprite.getHeight());




        batch.end();

        character.render(camera, environment);




        debugRenderVec(charCurrPos, character.getResultantActingForce().scl(0.4f), Color.BROWN);

        Vector2 boosterForce = character.getForce(boosterFUid);
        if(boosterForce != null){
            debugRenderVec(charCurrPos, boosterForce.cpy().scl(0.4f), Color.BLUE);
        }
//        debugRenderVec(charPos, character.velocity, Color.GREEN);

        if (collResult.colliding) {
            debugRenderVec(charCurrPos, colNormVec.cpy().scl(0.3f), Color.RED);
            debugRenderPoint(collResult.closestPoint, Color.CHARTREUSE);

        }
//        debugRenderVec(character.position, terrainForceVec, Color.ORANGE);


//        textBatch.begin();
//        textBatch.setProjectionMatrix(camera.combined);
////        textBatch.getTransformMatrix().setToScaling(0.01f, 0.01f, 0.01f);
//
////        font.getData().scale(0.01f);
//        font.draw(textBatch, "HELLO", charPos.x, charPos.y);
//        textBatch.end();


    }


    private void debugRenderVec(Vector2 start, Vector2 vec, Color color) {
        shapeRenderer.setProjectionMatrix(camera.combined);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(color);
        shapeRenderer.line(start.x, start.y, start.x + vec.x, start.y + vec.y);
        shapeRenderer.end();
    }

    private void debugRenderPoint(Vector2 point, Color color) {
        shapeRenderer.setProjectionMatrix(camera.combined);

        float width = 0.03f, height = 0.03f;

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(color);
        shapeRenderer.rect(point.x - width / 2f, point.y - height / 2f, width, height);
        shapeRenderer.end();
    }


    @Override
    public void pause() {


    }

    @Override
    public void dispose() {
        character.dispose();

        batch.dispose();
        bgTexture.dispose();
    }


    @Override
    public boolean keyDown(int keycode) {

        switch (keycode) {
            case 21: {
                //LEFT
                Gdx.app.log("INPUT", "LEFT PRESSED");

                Vector2 oldPos = character.getPosition();
                oldPos.x -= CHAR_MOVE_INC;

                character.setPosition(oldPos);
            }
            break;

            case 19: {
                //UP
                Gdx.app.log("INPUT", "UP PRESSED");

                Vector2 oldPos = character.getPosition();
                oldPos.y += CHAR_MOVE_INC;

                character.setPosition(oldPos);
            }
            break;

            case 22: {
                //RIGHT
                Gdx.app.log("INPUT", "RIGHT PRESSED");

                Vector2 oldPos = character.getPosition();
                oldPos.x += CHAR_MOVE_INC;

                character.setPosition(oldPos);
            }
            break;

            case 20: {
                //DOWN
                Gdx.app.log("INPUT", "DOWN PRESSED");
                Vector2 oldPos = character.getPosition();
                oldPos.y -= CHAR_MOVE_INC;

                character.setPosition(oldPos);
            }
            break;


        }

        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {


        if (!ACCELERATING) {
            ACCELERATING = true;

            Gdx.app.log("TAP", screenX + ":" + screenY);
            boosterFUid = this.character.setForce(boosterFUid, new Vector2(
                            screenX - Gdx.graphics.getWidth() / 2,
                            Gdx.graphics.getHeight() / 2 - screenY
                    ).nor().scl(4.0f)
            );
        }

        if (pointer < 5) {
            touches.get(pointer).touchX = screenX;
            touches.get(pointer).touchY = screenY;
            touches.get(pointer).touched = true;
        }

        character.controller.setAnimation("tail|idle_tail", 3, new AnimationController.AnimationListener() {

            @Override
            public void onEnd(AnimationController.AnimationDesc animation) {
                // this will be called when the current animation is done.
                // queue up another animation called "balloon".
                // Passing a negative to loop count loops forever.  1f for speed is normal speed.
                character.controller.queue("tail|hit_tail", -1, 2f, null, 0.1f);
            }

            @Override
            public void onLoop(AnimationController.AnimationDesc animation) {
                // TODO Auto-generated method stub


                Gdx.app.log("LOOP", "Iteration passed.");

            }

        });
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        ACCELERATING = false;
        Gdx.app.log("TAP", "UP");
        this.character.removeForce(boosterFUid);

        if (pointer < 5) {
            touches.get(pointer).touchX = 0;
            touches.get(pointer).touchY = 0;
            touches.get(pointer).touched = false;
        }
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        // TODO Auto-generated method stub
        return false;
    }
}
