package com.supergdx.game.math;

/**
 * Created by alexey.moroz on 3/19/18.
 */

public interface MFunction {

    /**
     * Returns a val of a function f(x).
     * @param x coordinate.
     * @return y val.
     */
    float val(float x);


    /**
     * Returns a val of first order derivative at point with X coordinate.
     * @param x
     * @return
     */
    float der(float x);

    /**
     * Returns a val of second order derivative at point with X coordinate.
     * @param x
     * @return
     */
    float der2(float x);
}
