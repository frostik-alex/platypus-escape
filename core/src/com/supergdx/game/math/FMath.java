package com.supergdx.game.math;

import com.supergdx.game.math.functions.ComplexSineFunction;
import com.supergdx.game.math.functions.SineFunction;

import java.util.List;

/**
 * Created by alexey.moroz on 3/19/18.
 */

public class FMath {

    public static MFunction getParametricSinFunction(final float amp, final float omega, final float phase, final float rise){

        return new MFunction() {
            @Override
            public float val(float x) {
                return (float)(amp * Math.sin(omega * x + phase) + rise);
            }

            @Override
            public float der(float x) {
                return (float)(amp * omega * Math.cos(omega * x + phase) + rise);
            }

            @Override
            public float der2(float x) {
                return (float)(-amp * omega * omega * Math.sin(omega * x + phase) + rise);
            }
        };
    }


    public static SineFunction generateSineFunction(float amplitude, float frequency, float phase, float shift){
        return new SineFunction(amplitude, frequency, phase, shift);
    }


    public static MFunction getShiftedSinFunction(){
        return getParametricSinFunction( 0.5f, 1.0f, 0.0f, 0.5f);
    }


    public static ComplexSineFunction generateComplexSineFunction(float[] params){
        return new ComplexSineFunction(
            generateSineFunction(params[0], params[1], params[2], params[3]),
            generateSineFunction(params[4], params[5], params[6], params[7]),
            generateSineFunction(params[8], params[9], params[10], params[11]),
            generateSineFunction(params[12], params[13], params[14], params[15])
        );
    }



    public static float gcd(float a, float b)
    {
        if (a < b)
            return gcd(b, a);

        // base case
        if (Math.abs(b) < 0.001)
            return a;

        else
            return (gcd(b, a -
                    (float)Math.floor(a / b) * b));
    }


    // Returns LCM of array elements
    public static float findLCM(List<Float> floatList)
    {
        // Initialize result
        float ans = floatList.get(0);

        // ans contains LCM of arr[0], ..arr[i]
        // after i'th iteration,
        for (int i = 1; i < floatList.size(); i++){
            float element = floatList.get(i);

            ans = (((element * ans)) / (gcd(element, ans)));
        }

        return ans;
    }


}
