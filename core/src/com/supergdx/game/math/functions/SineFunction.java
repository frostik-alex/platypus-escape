package com.supergdx.game.math.functions;

import com.supergdx.game.math.MFunction;
import com.supergdx.game.math.ParametrizableFunction;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;


/**
 * Created by alexey.moroz on 3/20/18.
 */

public class SineFunction implements MFunction, ParametrizableFunction{

    private static final int PARAMS_COUNT = 4;
    private static final int FLOAT_BYTE_COUNT = 4;

    private final float amp;
    private final float freq;
    private final float phase;
    private final float shift;
    private FloatBuffer paramsFloatBuffer;


    public SineFunction(float amp, float freq, float phase, float shift) {
        this.amp = amp;
        this.freq = freq;
        this.phase = phase;
        this.shift = shift;

        initFloatBuffer();
    }


    private void initFloatBuffer(){
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(this.getParamsBufferSize());
        byteBuffer.order(ByteOrder.nativeOrder());

        paramsFloatBuffer = byteBuffer.asFloatBuffer();
        paramsFloatBuffer.put(amp);
        paramsFloatBuffer.put(freq);
        paramsFloatBuffer.put(phase);
        paramsFloatBuffer.put(shift);
        paramsFloatBuffer.position(0);
    }

    @Override
    public float val(float x) {
        return (float) (amp * Math.sin( freq * x + phase) + shift);
    }

    @Override
    public float der(float x) {
        return (float) (amp * freq * Math.cos( freq * x + phase));
    }

    @Override
    public float der2(float x) {
        return (float) ( -amp * freq * freq * Math.sin( freq * x + phase));
    }



    @Override
    public int getParamsBufferSize() {
        return PARAMS_COUNT * FLOAT_BYTE_COUNT;
    }

    /**
     * Return FloatBuffer containing parameters that describe this sine wave.
     * Order: [Amplitude, Frequency, Phase, Constant]
     *
     *
     * @return Read-only float buffer.
     */
    public FloatBuffer getParamsBuffer(){
        //return read-only buffer in order to prevent occasional changes.
        return paramsFloatBuffer.asReadOnlyBuffer();
    }

}
