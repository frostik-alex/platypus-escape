package com.supergdx.game.math;

import java.util.Iterator;
import java.util.List;

/**
 * Created by alexey.moroz on 3/19/18.
 */

public class MFunctionSum implements MFunction, Iterable<MFunction> {

    private List<MFunction> funcList;

    public MFunctionSum(List<MFunction> funcList){
        this.funcList = funcList;
    }

    @Override
    public float val(float x) {

        float res = 0.0f;

        for(MFunction func: funcList){
            res += func.val(x);
        }

        return res;
    }

    @Override
    public float der(float x) {
        float res = 0.0f;

        for(MFunction func: funcList){
            res += func.der(x);
        }

        return res;
    }

    @Override
    public float der2(float x) {
        float res = 0.0f;

        for(MFunction func: funcList){
            res += func.der2(x);
        }

        return res;
    }

    @Override
    public Iterator<MFunction> iterator() {
        return funcList.iterator();
    }
}
