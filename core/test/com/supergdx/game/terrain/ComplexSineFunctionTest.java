package com.supergdx.game.terrain;

import com.supergdx.game.math.FMath;
import com.supergdx.game.math.functions.ComplexSineFunction;

import org.junit.Test;

import java.nio.FloatBuffer;

import static org.junit.Assert.*;

/**
 * Created by alexey.moroz on 3/20/18.
 */
public class ComplexSineFunctionTest {


    private ComplexSineFunction target;


    @Test
    public void shouldGenerateCorrectBuffer() {

        target = FMath.generateComplexSineFunction(new float[]{
                1.0f, 2.0f, 3.0f, 4.0f,
                11.0f, 12.0f, 13.0f, 14.0f,
                21.0f, 22.0f, 23.0f, 24.0f,
                31.0f, 32.0f, 33.0f, 34.0f
        });


        FloatBuffer fb = target.getParamsBuffer();
        fb.position(0);

        assertEquals(16, fb.capacity());
        assertEquals(16, fb.limit());
        assertEquals(16, fb.remaining());


        assertEquals(1.0f, fb.get(), 0.000001f);
        assertEquals(2.0f, fb.get(), 0.000001f);
        assertEquals(3.0f, fb.get(), 0.000001f);
        assertEquals(4.0f, fb.get(), 0.000001f);


        assertEquals(11.0f, fb.get(), 0.000001f);
        assertEquals(12.0f, fb.get(), 0.000001f);
        assertEquals(13.0f, fb.get(), 0.000001f);
        assertEquals(14.0f, fb.get(), 0.000001f);


        assertEquals(21.0f, fb.get(), 0.000001f);
        assertEquals(22.0f, fb.get(), 0.000001f);
        assertEquals(23.0f, fb.get(), 0.000001f);
        assertEquals(24.0f, fb.get(), 0.000001f);


        assertEquals(31.0f, fb.get(), 0.000001f);
        assertEquals(32.0f, fb.get(), 0.000001f);
        assertEquals(33.0f, fb.get(), 0.000001f);
        assertEquals(34.0f, fb.get(), 0.000001f);
    }
}