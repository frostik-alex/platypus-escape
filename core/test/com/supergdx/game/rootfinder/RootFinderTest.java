package com.supergdx.game.rootfinder;

import com.supergdx.game.math.MFunction;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alexey.moroz on 3/20/18.
 */
public class RootFinderTest {


    @Test
    public void shouldFindRoot_linearEq() {

        MFunction linear = new MFunction() {
            @Override
            public float val(float x) {
                return 5*x + 20;
            }

            @Override
            public float der(float x) {
                return 5;
            }

            @Override
            public float der2(float x) {
                return 0;
            }
        };


        RootResult result = RootFinder.newtonsApproxRoot(linear, 0.0f, 0.01f);

        assertEquals(RootStatus.OK, result.getStatus());
        assertEquals(-4.0f, result.getRootValue(), 0.01f);
    }



    @Test
    public void shouldFindRoot_sineEq() {

        MFunction linear = new MFunction() {
            @Override
            public float val(float x) {
                return (float)Math.sin(x);
            }

            @Override
            public float der(float x) {
                return (float) Math.cos(x);
            }

            @Override
            public float der2(float x) {
                return 0;
            }
        };


        RootResult result = RootFinder.newtonsApproxRoot(linear, 1.0f, 0.01f);

        assertEquals(RootStatus.OK, result.getStatus());
        assertEquals(0.0f, result.getRootValue(), 0.01f);
    }


    @Test
    public void shouldFindRoot_complexEq() {

        MFunction linear = new MFunction() {
            @Override
            public float val(float x) {
                return (float) ( 5* x*x*x + 0.2 * Math.sin(5*x) - 3 * x + 10);
            }

            @Override
            public float der(float x) {
                return (float)( 15*x*x + Math.cos(5*x) -3);
            }

            @Override
            public float der2(float x) {
                return (float)(30*x - 5*Math.sin(5*x));
            }
        };


        RootResult result = RootFinder.newtonsApproxRoot(linear, 0.0f, 0.00001f);

        assertEquals(RootStatus.OK, result.getStatus());
        assertEquals(-1.41272f, result.getRootValue(), 0.00001f);
    }


    @Test
    public void shouldFindRoot_transcendentEq() {

        MFunction linear = new MFunction() {
            @Override
            public float val(float x) {
                return (float) ( Math.cos(x) - x);
            }

            @Override
            public float der(float x) {
                return (float)( -Math.sin(x) - 1);
            }

            @Override
            public float der2(float x) {
                return (float)(-Math.cos(x));
            }
        };


        RootResult result = RootFinder.newtonsApproxRoot(linear, 0.0f, 0.000001f);

        assertEquals(RootStatus.OK, result.getStatus());
        assertEquals(0.739085f, result.getRootValue(), 0.000001f);
    }
}