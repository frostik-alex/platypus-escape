#ifdef GL_ES
    precision highp float;
#endif


const float COVER_MIN = 0.0;
const float COVER_MAX = 0.2;

const float TERRAIN_SCALE = 6.0;

const float PI = 3.141592653589793238;
const float TWO_PI = 6.283185307179586476925286766559;


uniform sampler2D u_texture;
uniform sampler2D u_terrain_texture;
uniform sampler2D u_terrain_cover_texture;

uniform mat4 u_projTrans;


uniform vec2 u_character_pos;
uniform float u_character_radius;

uniform vec2 u_cam_pos;
uniform float u_cam_zoom;


uniform vec2 u_resolution;
uniform float u_aspect_ratio;
uniform float u_time;

uniform mat4 u_terrain[16];

varying vec4 v_color;
varying vec2 v_texCoords;



float circle(vec2 pos, vec2 circlePos, float r){
    float dx = pos.x - circlePos.x;
    float dy = pos.y - circlePos.y;

    return step(  dx*dx + dy*dy,  r*r );
}



float pSin(float x, float a, float t, float p, float c){
    return a * sin( mod(t * x + p, TWO_PI) ) + c;
}


float pSin(float x, vec4 params){
    return params.x * sin( mod(params.y * x + params.z, TWO_PI) ) + params.w;
}

float pComplexSin(float x, mat4 params){

    float amplitude = pSin(x, params[0]);
    float freq = pSin(x, params[1]);
    float phase = pSin(x, params[2]);
    float shift = pSin(x, params[3]);

    float value = pSin(x, amplitude, freq, phase, shift);

    return value;
}

float getTerrainHeight(mat4 terrainParams, float posX){
    return pComplexSin(posX, terrainParams);
}



float _mod(float x, float y){
    return x - y * floor(x/y);
}


void main() {
    //pixel position
    vec2 uv =  2.0 * gl_FragCoord.xy / u_resolution.xy - 1.0 ;

    uv.x = uv.x + step(u_resolution.y, u_resolution.x) * uv.x * (u_aspect_ratio - 1.0);
    uv.y = uv.y + step(u_resolution.x, u_resolution.y) * uv.y * (u_aspect_ratio - 1.0);


    float zoomUp = 1.0 - step(u_cam_zoom, 1.0);
    float invZoom = 1.0/u_cam_zoom;


   float scale = invZoom + zoomUp * (1.0 - 2.0 * invZoom);
   mat3 scaleMat = mat3(
        scale, 0.0, 0.0,
        0.0, scale, 0.0,
        0.0, 0.0, 1.0
    );

    mat3 translateMat = mat3(
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        u_cam_pos.x, u_cam_pos.y, 1.0
    );



    vec3 worldCoords = translateMat * scaleMat * vec3(uv, 1.0);


    vec3 color = texture2D(u_texture, v_texCoords).rgb;
    float gray = (color.r + color.g + color.b) / 7.0;
    vec3 grayscale = vec3(gray);

    float treshold = 0.0;
    for(int i=0; i < 16; i++){
        treshold += getTerrainHeight(u_terrain[i], worldCoords.x);
    }

    //terrain texture
    float applyTerrain = step(treshold, worldCoords.y);
    vec2 terrainTexCoord = vec2(worldCoords.x, worldCoords.y - TERRAIN_SCALE/2.0) / TERRAIN_SCALE;
    vec3 terrainColor = texture2D(u_terrain_texture, terrainTexCoord).rgb;
    vec3 resColor = mix(terrainColor, color, applyTerrain);


    vec2 coverCoord = vec2(worldCoords.x, treshold - worldCoords.y);
    float applyCover = step(COVER_MIN, coverCoord.y) * step(coverCoord.y, COVER_MAX);
    coverCoord.y = (coverCoord.y - COVER_MIN) / (COVER_MAX - COVER_MIN);
    vec4 coverColor = texture2D(u_terrain_cover_texture, coverCoord);
    resColor = mix(resColor, coverColor.rgb, applyCover * coverColor.a);



    float applyCircle = circle(worldCoords.xy, u_character_pos, u_character_radius);
    resColor = mix(resColor, vec3(1.0, 0.0, 0.0), applyCircle * 0.35);


    gl_FragColor = vec4(resColor, 1.0);
}